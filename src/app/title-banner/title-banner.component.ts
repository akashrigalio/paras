import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title-banner',
  templateUrl: './title-banner.component.html',
  styleUrls: ['./title-banner.component.css']
})
export class TitleBannerComponent implements OnInit {
  @Input() name: any;
  @Input() image: any;
  @Input() bgColor: any;
  @Input() textColor: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.bgColor);
  }

}
