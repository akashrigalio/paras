import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerSurviorStoryComponent } from './cancer-survior-story.component';

describe('CancerSurviorStoryComponent', () => {
  let component: CancerSurviorStoryComponent;
  let fixture: ComponentFixture<CancerSurviorStoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancerSurviorStoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerSurviorStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
