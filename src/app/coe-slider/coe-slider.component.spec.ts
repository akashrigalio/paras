import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoeSliderComponent } from './coe-slider.component';

describe('CoeSliderComponent', () => {
  let component: CoeSliderComponent;
  let fixture: ComponentFixture<CoeSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoeSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoeSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
