import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentChildHealthCheckupComponent } from './treatment-child-health-checkup.component';

describe('TreatmentChildHealthCheckupComponent', () => {
  let component: TreatmentChildHealthCheckupComponent;
  let fixture: ComponentFixture<TreatmentChildHealthCheckupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreatmentChildHealthCheckupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentChildHealthCheckupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
