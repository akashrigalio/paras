import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurHospitalsComponent } from './our-hospitals.component';

describe('OurHospitalsComponent', () => {
  let component: OurHospitalsComponent;
  let fixture: ComponentFixture<OurHospitalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurHospitalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurHospitalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
