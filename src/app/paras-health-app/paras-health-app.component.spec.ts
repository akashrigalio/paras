import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParasHealthAppComponent } from './paras-health-app.component';

describe('ParasHealthAppComponent', () => {
  let component: ParasHealthAppComponent;
  let fixture: ComponentFixture<ParasHealthAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParasHealthAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParasHealthAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
