import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-paras-health-app',
  templateUrl: './paras-health-app.component.html',
  styleUrls: ['./paras-health-app.component.css']
})
export class ParasHealthAppComponent implements OnInit {

  customTitle:any = {};
  customImage:any = {};
  customBgColor:any = {};
  customTxtColor:any = {};

  constructor() { }

  ngOnInit(): void {
    this.customTitle = "Paras Health Mate App";
    this.customImage = "assets/images/paras-app-banner-image.jpg";
    this.customBgColor = "#00BBF2";
    this.customTxtColor = "#000000";
  }

}
