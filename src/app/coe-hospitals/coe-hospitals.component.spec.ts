import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoeHospitalsComponent } from './coe-hospitals.component';

describe('CoeHospitalsComponent', () => {
  let component: CoeHospitalsComponent;
  let fixture: ComponentFixture<CoeHospitalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoeHospitalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoeHospitalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
