import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurAchivementComponent } from './our-achivement.component';

describe('OurAchivementComponent', () => {
  let component: OurAchivementComponent;
  let fixture: ComponentFixture<OurAchivementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurAchivementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurAchivementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
