import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infographics-video',
  templateUrl: './infographics-video.component.html',
  styleUrls: ['./infographics-video.component.css']
})
export class InfographicsVideoComponent implements OnInit {

  constructor() { }

  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = "Infographics & Helpful Videos"
  }

}