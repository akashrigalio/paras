import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfographicsVideoComponent } from './infographics-video.component';

describe('InfographicsVideoComponent', () => {
  let component: InfographicsVideoComponent;
  let fixture: ComponentFixture<InfographicsVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfographicsVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfographicsVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
